import {library} from '@fortawesome/fontawesome-svg-core'

import {faFillDrip} from '@fortawesome/pro-duotone-svg-icons'

library.add(faFillDrip)
