import {library} from '@fortawesome/fontawesome-svg-core'

import {faOpenid} from '@fortawesome/free-brands-svg-icons'

library.add(faOpenid)
