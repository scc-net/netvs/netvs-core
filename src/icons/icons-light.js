import {library} from '@fortawesome/fontawesome-svg-core'

import {
  faRectanglePro,
  faSquarePlus
} from '@fortawesome/pro-light-svg-icons'

library.add(faRectanglePro)
library.add(faSquarePlus)
