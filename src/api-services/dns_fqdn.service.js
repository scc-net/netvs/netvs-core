import TransactionService from './transaction.service'

export default {
  getByParent(config, parent) {
    const ta = [
      { idx: 'parent_fqdn', name: 'dns.fqdn.list', old: { value: parent } },
      { name: 'dns.fqdn.list', inner_join_ref: { parent_fqdn: 'api_fkey_dns_fqdn_parent' } }
    ]
    return TransactionService.execute(config, ta)
  },
  getRecordsByTargetFQDN(config, fqdn) {
    const ta = [
      { name: 'dns.fqdn.list', old: { value: fqdn } },
      { name: 'dns.record.list', inner_join_ref: { fqdn_list: 'api_fkey_dns_record_target_fqdn' }, old: {is_auth: true} },
      { idx: 'rec_fqdns', name: 'dns.fqdn.list', inner_join_ref: { record_list: 'default' } },
      { name: 'dnscfg.fqdn_type.list', old: { sorting_params_list: ['position'] } },
      { name: 'dnscfg.record_inttype.list', old: { is_own: true, sorting_params_list: ['record_type'] } },
      {
        name: 'nd.ip_subnet.list',
        inner_join_ref: { record_list: 'api_func_dns_record_rr_chain_target_contains_subnet' },
        old: { sorting_params_list: ['type ASC', 'cidr ASC'] }
      }
    ]
    return TransactionService.execute(config, ta)
  },
  getRecordsByFQDN(config, fqdn) {
    const ta = [
      { name: 'dns.fqdn.list', old: { value: fqdn } },
      { name: 'dns.record.list', old: { sorting_params_list: ['type', 'fqdn'], is_auth: true }, inner_join_ref: { fqdn_list: 'default' } },
      { name: 'dnscfg.fqdn_type.list', old: { sorting_params_list: ['position'] } },
      { name: 'dnscfg.record_inttype.list', old: { is_own: true, sorting_params_list: ['record_type'] } },
      {
        name: 'nd.ip_subnet.list',
        inner_join_ref: { record_list: 'api_func_dns_record_rr_chain_target_contains_subnet' },
        old: { sorting_params_list: ['type ASC', 'cidr ASC'] }
      }
    ]
    return TransactionService.execute(config, ta)
  },
  getRecordsByIP(config, ip) {
    const ta = [
      { name: 'dns.ip_addr.list', old: { value_cidr: ip } },
      { name: 'dns.record.list', old: { sorting_params_list: ['type', 'fqdn'], is_auth: true }, inner_join_ref: {ip_addr_list: 'default'}},
      { name: 'dnscfg.fqdn_type.list', old: { sorting_params_list: ['position'] } },
      { name: 'dns.fqdn.list', inner_join_ref: { record_list: 'default' } },
      { name: 'dnscfg.record_inttype.list', old: { is_own: true, sorting_params_list: ['record_type'] } },
      {
        name: 'nd.ip_subnet.list',
        inner_join_ref: { ip_addr_list: 'default' },
        old: { sorting_params_list: ['type ASC', 'cidr ASC'] }
      }
    ]
    return TransactionService.execute(config, ta)
  },
  getOwnFQDNs(config) {
    const ta = [
      { name: 'cntl.mgr2ou.list', old: { is_own: true } },
      { name: 'cntl.mgr2group.list', old: { is_own: true } },
      { name: 'org.unit.list', inner_join_ref: { mgr2ou_list: 'default' } },
      { name: 'cntl.group.list', inner_join_ref: { mgr2group_list: 'default' } },
      { name: 'dns.fqdn2ou.list', inner_join_ref: { unit_list: 'default'} },
      { name: 'dns.fqdn2group.list', inner_join_ref: { group_list: 'default'} },
      { idx: 'ou_fqdns', name: 'dns.fqdn.list', inner_join_ref: { fqdn2ou_list: 'default' } },
      { idx: 'group_fqdns', name: 'dns.fqdn.list', inner_join_ref: { fqdn2group_list: 'default' } }
    ]
    return TransactionService.execute(config, ta)
  }
}
