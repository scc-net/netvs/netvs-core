import Axios from 'axios'

export default {
  submit_message(state, cidr, message) {
    const config = ('netdb_axios_config' in state) ? state.netdb_axios_config : state
    config.headers['Content-Type'] = 'application/json; charset=utf-8'
    return Axios.post('/api/ip_contact', { cidr: cidr, message: message }, config)
  }
}
