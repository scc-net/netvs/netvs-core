import Axios from 'axios'

export default {
  getLeases (config, bcd) {
    return Axios.get(`/api/dhcp_leases/bcds/${bcd}/active`, config.netdb_axios_config)
  }
}
