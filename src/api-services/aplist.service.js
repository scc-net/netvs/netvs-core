import Axios from 'axios'

export default {
  fetchList(config) {
    return Axios.get('/api/apliste/json', config.netdb_axios_config)
  }
}
