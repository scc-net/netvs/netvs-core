import TransactionService from './transaction.service'

export default {
  list(config, bldg_number, room_number) {
    const ta = [
      { name: 'nd.module.list', old: { bldg: bldg_number, room: room_number } }, // Module
      { name: 'nd.slot.list', inner_join_ref: { module_list: 'api_fkey_nd_slot_parent_mdl' } } // Join Slots
    ]
    return TransactionService.execute(config, ta)
  }
}
