import Axios from 'axios'

export default {
  search_domain(state, fqdn) {
    const config = ('netdb_axios_config' in state) ? state.netdb_axios_config : state
    return Axios.post('/api/domain_finder', { fqdn: fqdn.toLowerCase() }, config)
  }
}
