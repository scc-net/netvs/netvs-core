export default {
  ipv6_to_ints(v6) {
    const doubleBytes = v6.split(':')
    const res = []
    for (let i = 0; i < 4; i++) {
      res.push(parseInt('0x' + doubleBytes[2 * i] + doubleBytes[2 * i + 1], 16))
    }
    return res
  },
  ip_to_int(ip) {
    let byteString = '0x'
    const bytes = ip.split('.')
    for (let i = 0; i < 4; i++) {
      let res = Number(bytes[i]).toString(16)
      if (res.length === 1) {
        res = '0' + res
      }
      byteString += res
    }
    return parseInt(byteString, 16)
  },
  is_ip_v4(ip) {
    return /^(\d|.)+$/.test(ip) && ip.split('.').length === 4 && ip.split('.').map((x) => parseInt(x, 10)).every((x) => x >= 0 && x <= 255)
  },
  ipv4_subnet_size(n) {
    return Math.pow(2, 32 - Number(n))
  },
  ip_num_addr(net) {
    return this.ipv4_subnet_size(Number(net.split('/')[1]))
  },
  ipv6_num_addr(net6) {
    return Math.pow(2, 128 - Number(net6.split('/')[1]))
  },
  ip_net_get_last(net) {
    return this.int_to_ip(this.ip_to_int(net.split('/')[0]) + this.ip_num_addr(net) - 1)
  },
  ip_net_get_first(net) {
    return net.split('/')[0]
  },
  ip_net_get_mask(net) {
    const s = parseInt(net.split('/')[1])
    return this.int_to_ip(parseInt('1'.repeat(s) + '0'.repeat(32 - s), 2))
  },
  int_to_ip(ip) {
    let bytestring = ip.toString(16)
    while (bytestring.length < 8) {
      bytestring = '0' + bytestring
    }
    const bytes = []
    for (let i = 0; i < 8; i++) {
      bytes.push(parseInt(bytestring.substr(i, 2), 16))
      i++
    }
    return bytes.join('.')
  },
  diff_ip4(a, b) {
    const a_ip = this.ip_to_int(b)
    const b_ip = this.ip_to_int(a)
    if (a_ip > b_ip) {
      return a_ip - b_ip
    }
    return b_ip - a_ip
  },
  compare(a, b) {
    if (this.is_ip_v4(a) && this.is_ip_v4(b)) {
      return this.ip_to_int(a) - this.ip_to_int(b)
    }
    const ip6a = this.ipv6_to_ints(a)
    const ip6b = this.ipv6_to_ints(b)
    for (let i = 0; i < 4; i++) {
      const r = ip6a[i] - ip6b[i]
      if (r === 0) {
        continue
      }
      return r
    }
    return 0
  },
  is_ptr(fqdn) {
    const parts = fqdn.split('.')
    parts.pop()
    if (parts.pop() !== 'arpa') {
      return false
    }
    const part = parts.pop()
    if (part !== 'in-addr' && part !== 'ip6') {
      return false
    }
    return true
  },
  ptr_to_addr(fqdn) {
    if (!this.is_ptr(fqdn)) {
      throw TypeError('FQDN not PTR!')
    }
    const parts = fqdn.split('.')
    parts.pop()
    parts.pop()
    const isLegacy = parts.pop() === 'in-addr'
    if (isLegacy) {
      return parts.reverse().join('.')
    }
    let res = ''
    parts.reverse()
    for (let i = 0; i < parts.length; i++) {
      if ((i % 4) === 0 && i !== 0) {
        res += ':'
      }
      res += parts[i]
    }
    return res
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  is_ip_v6(ip) {
    // Ensure the input are well formatted for the check
    ip = ip.toLowerCase()
    const ip_blocks = []
    const blocks = ip.split('::')
    // Check if there is maximum one '::' block inside the address
    if (blocks.length > 2) {
      return false
    } else if (blocks.length === 2) {
      // Check if there is a valid number of : in each section
      if (blocks[0].startsWith(':') || blocks[0].endsWith(':') || blocks[1].startsWith(':') || blocks[1].endsWith(':')) {
        return false
      }
      // Get blocks
      ip_blocks.push(...(blocks[0].split(':').filter(a => a)))
      ip_blocks.push(...(blocks[1].split(':').filter(a => a)))

      if (ip_blocks.length > 7) {
        return false
      }
    } else if (blocks.length === 1) {
      // Get blocks
      ip_blocks.push(...(blocks[0].split(':').filter(a => a)))
      if (ip_blocks.length !== 8) {
        return false
      }
    }

    // Check the single blocks
    for (const block of ip_blocks) {
      const match = block.match(/^[a-f0-9]{1,4}$/i)
      if (!match || match[0] !== block) {
        return false
      }
    }
    // Return true if all the checks pass
    return true
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  normalize_ipv6(ip) {
    ip = ip.toLowerCase()

    // Check if the input is a valid ip
    if (!this.is_ip_v6(ip)) {
      return ''
    }

    const nh = ip.split(/::/g)
    if (nh.length > 2) {
      return ''
    }

    let sections = []
    if (nh.length === 1) {
      // full mode
      sections = ip.split(/:/g)
      if (sections.length !== 8) {
        return ''
      }
    } else if (nh.length === 2) {
      // compact mode
      const n = nh[0]
      const h = nh[1]
      const ns = n.split(/:/g)
      const hs = h.split(/:/g)
      for (const i in ns) {
        sections[i] = ns[i]
      }
      for (let i = hs.length; i > 0; --i) {
        sections[7 - (hs.length - i)] = hs[i - 1]
      }
    }
    for (let i = 0; i < 8; ++i) {
      if (sections[i] === undefined) {
        sections[i] = '0000'
      }
      sections[i] = this._leftPad(sections[i], '0', 4)
    }
    return sections.join(':')
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  abbreviate_ipv6(ip) {
    if (!this.is_ip_v6(ip)) {
      return ''
    }
    ip = this.normalize_ipv6(ip)
    ip = ip.replace(/0000/g, 'g')
    ip = ip.replace(/(^|:)000/g, '$1')
    ip = ip.replace(/(^|:)00/g, '$1')
    ip = ip.replace(/(^|:)0/g, '$1')
    ip = ip.replace(/g/g, '0')
    const sections = ip.split(/:/g)
    let zPreviousFlag = false
    let zeroStartIndex = -1
    let zeroLength = 0
    let zStartIndex = -1
    let zLength = 0
    for (let i = 0; i < 8; ++i) {
      const section = sections[i]
      const zFlag = (section === '0')
      if (zFlag && !zPreviousFlag) {
        zStartIndex = i
      }
      if (!zFlag && zPreviousFlag) {
        zLength = i - zStartIndex
      }
      if (zLength > 1 && zLength > zeroLength) {
        zeroStartIndex = zStartIndex
        zeroLength = zLength
      }
      zPreviousFlag = (section === '0')
    }
    if (zPreviousFlag) {
      zLength = 8 - zStartIndex
    }
    if (zLength > 1 && zLength > zeroLength) {
      zeroStartIndex = zStartIndex
      zeroLength = zLength
    }
    if (zeroStartIndex >= 0 && zeroLength > 1) {
      sections.splice(zeroStartIndex, zeroLength, 'g')
    }
    ip = sections.join(':')
    ip = ip.replace(/:g:/g, '::')
    ip = ip.replace(/:g/g, '::')
    ip = ip.replace(/g:/g, '::')
    ip = ip.replace(/g/g, '::')
    return ip
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  _leftPad(d, p, n) {
    const padding = p.repeat(n)
    if (d.length < padding.length) {
      d = padding.substring(0, padding.length - d.length) + d
    }
    return d
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  _hex2bin(hex) {
    return parseInt(hex, 16).toString(2)
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  _bin2hex(bin) {
    return parseInt(bin, 2).toString(16)
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  _addr2bin(addr) {
    const nAddr = this.normalize_ipv6(addr)
    const sections = nAddr.split(':')
    let binAddr = ''
    for (const section of sections) {
      binAddr += this._leftPad(this._hex2bin(section), '0', 16)
    }
    return binAddr
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  _bin2addr(bin) {
    const addr = []
    for (let i = 0; i < 8; ++i) {
      const binPart = bin.substr(i * 16, 16)
      const hexSection = this._leftPad(this._bin2hex(binPart), '0', 4)
      addr.push(hexSection)
    }
    return addr.join(':')
  },
  // Adapted function from https://github.com/elgs/ip6 . Copyright (c) 2016 Qian Chen, licensed under MIT-License
  ipv6_range(addr, mask0, mask1, abbr) {
    this.is_ip_v6(addr)
    mask0 *= 1
    mask1 *= 1
    mask1 = mask1 || 128
    if (mask0 < 0 || mask1 < 0 || mask0 > 128 || mask1 > 128 || mask0 > mask1) {
      return {
        start: '',
        end: '',
        size: 0
      }
    }
    const binAddr = this._addr2bin(addr)
    const binNetPart = binAddr.substr(0, mask0)
    const binHostPart = '0'.repeat(128 - mask1)
    const binStartAddr = binNetPart + '0'.repeat(mask1 - mask0) + binHostPart
    const binEndAddr = binNetPart + '1'.repeat(mask1 - mask0) + binHostPart
    if (abbr) {
      return {
        start: this.abbreviate_ipv6(this._bin2addr(binStartAddr)),
        end: this.abbreviate_ipv6(this._bin2addr(binEndAddr)),
        size: Math.pow(2, mask1 - mask0)
      }
    } else {
      return {
        start: this._bin2addr(binStartAddr),
        end: this._bin2addr(binEndAddr),
        size: Math.pow(2, mask1 - mask0)
      }
    }
  },
  check_ipv6_subnet(subnet) {
    // Ensure the input is in lower case
    subnet = subnet.toLowerCase()

    // Split by the separator
    subnet = subnet.split('/')

    // Check if there are exactly two parts
    if (subnet.length !== 2) {
      return false
    }
    // Check if the first part is a valid ip address and the other part is a valid integer between 1 and 128
    return this.is_ip_v6(subnet[0]) && parseInt(subnet[1]) > 0 && parseInt(subnet[1]) <= 128
  },
  is_mac_address(a) {
    // Ensure the right format
    a = a.toLowerCase()
    return !!a.match(/^([0-9a-f]{2}[:.-]?){6}$/i)
  },
  mac_to_local_link_ipv6(mac) {
    mac = mac.toLowerCase()
    if (!this.is_mac_address(mac)) {
      // Return an empty string if the input isn't valid
      return ''
    }
    // Remove the spacing characters from the string
    mac = mac.replace(/[:.-]/g, '').split('')
    // Add the fffe string to the middle of the array
    mac.splice(6, 0, 'fffe')
    // Flip the 7th bit of the address
    mac[1] = (parseInt(mac[1], 16) ^ 2).toString(16)
    // Convert array to local link ipv6 address
    return this.abbreviate_ipv6('fe80:' + mac.join('').replace(/(....)/g, ':$1'))
  },
  eui_ipv6_to_mac(local) {
    local = local.toLocaleString()
    local = this.normalize_ipv6(local).substring(20).replace(/:/g, '')
    // Check if the string contains the flag of the EUI-64
    if (local.substring(6, 10) === 'fffe') {
      // Split the string into parts
      local = local.split('')
      // Remove the fffe from the list
      local.splice(6, 4)
      // Flip the 7th bit again
      local[1] = (parseInt(local[1], 16) ^ 2).toString(16)
      // Concatenate the result
      return local.join('').replace(/(..)/g, ':$1').substring(1)
    }
    return ''
  },
  is_eui_ipv6(local) {
    local = local.toLowerCase()
    local = this.normalize_ipv6(local).substring(20).replace(/:/g, '')
    // Check if the string contains the flag of the EUI-64
    return local.substring(6, 10) === 'fffe'
  },
  compare_ipv6(a, b) {
    a = a.toLowerCase()
    b = b.toLowerCase()
    a = this.normalize_ipv6(a)
    b = this.normalize_ipv6(b)
    a = a.split(':')
    b = b.split(':')
    const results = []
    for (let i = 0; i < 8; i++) {
      const first = parseInt(a[i], 16)
      const second = parseInt(b[i], 16)
      results.push(first - second)
    }
    for (const result of results) {
      if (result > 0) {
        return 1
      } else if (result < 0) {
        return -1
      }
    }
    return 0
  }
}
