#!/usr/bin/env python3
from typing import Optional

import requests
import os, subprocess, sys

default_endpoint = os.environ.get('CI_COMMIT_BRANCH', 'test')
api_generator = os.environ.get('API_GENERATOR', '/venv/bin/net-api-generator')

if default_endpoint == 'devel':
    api_endpoint = os.environ.get('NETDB_DEVEL_ENDPOINT')
else:
    api_endpoint = os.environ.get('NETDB_ENDPOINT')

endpoint_protocol = os.environ.get('NETDB_ENDPOINT_PROTOCOL', 'https')

os.environ['NETDB_ENDPOINT'] = api_endpoint

data = requests.get('{}://{}'.format(endpoint_protocol, api_endpoint)).json()[0]

highest_api_version = None


def exec_command_into_file(command: list[str], file: Optional[str], env: dict[str, str | None]):
    p = subprocess.Popen(command, env=env, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, err = p.communicate()
    p.wait()
    if file is not None:
        with open(file, 'w') as f:
            f.write(stdout.decode('utf-8'))
    if len(err.decode('utf-8')) > 0:
        print(err.decode('utf-8'), file=sys.stderr)
        sys.exit(1)


for api_version in data:
    version_string = "{}.{}".format(api_version['major'], api_version['minor'])
    if highest_api_version is None or highest_api_version < version_string:
        highest_api_version = version_string
    e = dict(os.environ)
    e['NETDB_VERSION'] = version_string
    print('Generating API for version {}'.format(version_string))
    exec_command_into_file([api_generator, 'openapi', '--default_endpoint='+default_endpoint], 'public/api_{}.yml'.format(version_string.replace('.', '_')), e)

    print('Generating TA schema for version {}'.format(version_string))
    exec_command_into_file([api_generator, 'ta-schema'], 'public/netdb_{}_ta.yml'.format(version_string.replace('.', '_')), e)
