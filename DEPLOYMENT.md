# NETVS Deployment

Instances:
- [Prod](https://netvs.scc.kit.edu)
- [Test](https://netvs-test.scc.kit.edu)
- [Devel](https://netvs-devel.scc.kit.edu)
- [Devel-Oldrelease](https://netvs-devel-oldrelease.scc.kit.edu)
- [Lab](https://netvs-lab.scc.kit.edu)

```mermaid
flowchart LR
    NETDB_PROD[(Prod)]
    NETDB_TEST[(Test)]
    NETDB_DEVEL[(Devel)]
    Oldrel_inst[Oldrelease]
    Test_inst[fa:fa-vial Test]
    Lab_inst[fa:fa-vials Lab]
    Prod_inst[Prod]
    Devel_inst[fa:fa-dumpster-fire Devel]
    Devel{{`devel`}}
    Lab{{`lab`}}
    Main{{`prod`}}

    Devel-->D{Feature working\n&&\nNETVS Target API\nsupported by\nNETDB Prod}
    Devel <--> Lab
    D -->|Yes| Main
    D -->|No| Devel

    Lab -.->|deploys to| Lab_inst
    Devel -.->|deploys to| Devel_inst
    Main -.->|deploys to| Prod_inst & Oldrel_inst & Test_inst

    Lab_inst & Devel_inst & Oldrel_inst -.->|uses| NETDB_DEVEL
    Prod_inst -.->|uses| NETDB_PROD
    Test_inst -.->|uses| NETDB_TEST
```
