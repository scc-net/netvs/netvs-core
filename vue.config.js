module.exports = {
  devServer: {
    proxy: {
      '^/api/4.1': {
        target: 'https://api.netdb-devel.scc.kit.edu',
        changeOrigin: true,
        pathRewrite: { '^/api': '' },
      },
      '^/api/openapi.json': {
        target: 'https://netvs-devel.scc.kit.edu',
        // target: 'http://localhost:8000',
        changeOrigin: false,
        // pathRewrite: { '^/api': '' },
      },
      '^/api': {
        target: 'https://netvs-devel.scc.kit.edu',
        // target: 'http://localhost:8000',
        changeOrigin: false
      },
      '^/hub': {
        target: 'https://netvs-devel.scc.kit.edu',
        // target: 'http://localhost:5000
        changeOrigin: false
      }
    }
  },
  pluginOptions: {
    i18n: {
      locale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
}
