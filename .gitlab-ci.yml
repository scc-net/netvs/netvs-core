default:
  image: debian-bookworm:latest
  tags:
    - netvs
cache:
  key:
    files:
      - package-lock.json
  paths:
    - .npm_ci/
before_script:
  ## dependencies
  - apt-get update -y
  - command -v curl || apt-get -y install curl
  - command -v git || apt-get -y install git
  - apt-get -y install apt-transport-https gnupg2
  - test -e  /etc/apt/sources.list.d/nodesource.list || curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
  - test -e  /etc/apt/sources.list.d/nodesource.list || echo 'deb https://deb.nodesource.com/node_16.x bookworm main' > /etc/apt/sources.list.d/nodesource.list
  - "echo -e 'Package: *\nPin: origin deb.nodesource.com\nPin-Priority: 900' > /etc/apt/preferences.d/nodesource_pin"
  - if [ $CI_JOB_STAGE != "deploy" ]; then command -v npm || (apt-get update -y && apt-get install -y nodejs); fi
  - if [ $CI_JOB_STAGE != "deploy" ]; then command -v pip3 || (apt-get update -y && apt-get install -y python3-pip); fi
  - if [ $CI_JOB_STAGE != "deploy" ]; then (apt-get update -y && apt-get install -y python3-venv && python3 -m venv /venv && source /venv/bin/activate); fi
  - if [[ $CI_JOB_STAGE != "deploy" ]]; then if [[ $CI_COMMIT_BRANCH == "devel" ]]; then /venv/bin/pip3 install git+https://gitlab.kit.edu/scc-net/netvs/api-generator.git@devel#egg=net-api-generator; else /venv/bin/pip3 install git+https://gitlab.kit.edu/scc-net/netvs/api-generator.git@main#egg=net-api-generator; fi; fi
  - echo "${CI_COMMIT_SHORT_SHA},Job ID ${CI_JOB_ID}@${CI_RUNNER_DESCRIPTION}"
  - sed "s&__LOCAL_BUILD__&${CI_COMMIT_SHORT_SHA},Job ID ${CI_JOB_ID}@${CI_RUNNER_DESCRIPTION}&g" netvs.config.js.example > netvs.config.js
  - sed -i "s&__JOB_ID__&${CI_JOB_ID}&g" netvs.config.js
  - sed -i "s&__COMMIT_SHORT_SHA__&${CI_COMMIT_SHORT_SHA}&g" netvs.config.js
  - sed -i "s&__JOB_ID__&${CI_JOB_ID}&g" public/version.json
  - sed -i "s&__COMMIT_SHORT_SHA__&${CI_COMMIT_SHORT_SHA}&g" public/version.json
  - if [[ $CI_JOB_STAGE != "deploy" ]]; then npm ci --cache .npm_ci --prefer-offline; fi
  - export NETDB_VERSION="4.1"
stages:
  - build
  - lint
  - deploy
  - e2e

frontend-build:
  stage: build
  script:
      - python3 build_swagger.py
      - /venv/bin/net-api-generator es-webpack
      - npm run build
  artifacts:
      expire_in: 7 days
      paths:
          - dist/
          - src/api-services.gen/
  needs: []
  interruptible: true
frontend-lint:
  stage: lint
  script:
      - npm run lint-ci
  allow_failure: false
  needs: []
  interruptible: true

frontend-audit:
  stage: lint
  script:
      - npm audit --audit-level high
  allow_failure: true
  needs: []
  interruptible: true

deploy_lab:
  stage: deploy
  script:
    - 'command -v ssh-agent || ( apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh www-netvs@netvs-lab.scc.kit.edu "${CI_PIPELINE_ID}"
  environment:
    name: lab
    url: https://netvs-lab.scc.kit.edu
  only:
  - lab
  needs: ["frontend-build"]

deploy_devel:
  stage: deploy
  script:
    - 'command -v ssh-agent || ( apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh www-netvs@netvs-devel.scc.kit.edu "${CI_PIPELINE_ID}"
  environment:
    name: devel
    url: https://netvs-devel.scc.kit.edu
  only:
  - devel
  needs: ["frontend-build"]
  interruptible: false

deploy_test:
  stage: deploy
  script:
    - 'command -v ssh-agent || ( apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh www-netvs@netvs-test.scc.kit.edu "${CI_PIPELINE_ID}"
  environment:
    name: test
    url: https://netvs-test.scc.kit.edu
  only:
  - prod
  needs: ["frontend-build"]
  interruptible: false

deploy_prod:
  stage: deploy
  script:
    - 'command -v ssh-agent || ( apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh www-netvs@netvs.scc.kit.edu "${CI_PIPELINE_ID}"
  environment:
    name: prod
    url: https://netvs.scc.kit.edu
  only:
  - prod
  needs: ["frontend-build"]
  interruptible: false

e2e_chrome:
  stage: e2e
  script:
    - curl -s https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
    - echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/chrome.list
    - apt-get -y update && apt-get -y install google-chrome-stable
    - npm run serve 2> /dev/null &
    - sleep 45 && npx nightwatch --env chrome tests/specs
  needs: ["frontend-build"]
  allow_failure: true
  artifacts:
    when: always
    paths:
      - tests_output/
      - screens/
    reports:
      junit: tests_output/**/*.xml
  interruptible: true
#e2e_firefox:
#  stage: e2e
#  script:
#    - apt-get -y update && apt-get -y install firefox-esr
#    - cd frontend/
#    - npm run serve 2> /dev/null &
#    - sleep 45 && npx nightwatch --env firefox tests/specs
#  needs: ["frontend-build"]
#  artifacts:
#    when: always
#    paths:
#      - frontend/tests_output/
#      - frontend/screens/
#    reports:
#      junit: frontend/tests_output/**/*.xml
