# net-suite frontend

## Development setup

The initial setup consists of the following steps:

1. Install the following `pip`-module using `git` to be able to generate api configurations
   ```shell
   pip3 install git+https://gitlab.kit.edu/scc-net/netvs/api-generator.git@main#egg=net-api-generator
   ```
2. Enter the `frontend` folder using
   ```shell
   cd frontend/
   ```
3. Generate API-Endpoints in `frontend/public/api.yml` using
    ```
    net-api-generator openapi --default_endpoint=devel > public/api.yml
    ```
   Instead of creating the api configuration for `devel`, `test` can also be used.
   Additionally, you need generate the files for the `api-services.gen` folder using the following command
   ```shell
   net-api-generator es-webpack
   ```
   You can add a `--version=${API_VERSION}` parameter, if you need a specific version.
4. Create a netvs-configuration `netvs.config.js`. See `netvs.config.js.example` for more information. In most cases it's enough to simply copy and rename the example config.
5. 
   1. Set the `FONTAWESOME_NPM_AUTH_TOKEN` environment variable 
   2. Run the development server either with `docker` and the docker compose plugin (`docker compose`, recommended way) 
      ```shell
      docker-compose up -d netvs-core_run
      ```
      or by running a local server (which we discourage from) after installing the project using the following two commands
       ```shell
       npm install
       npm run serve
       ```

## Recommended Setup with Docker and JetBrains WebStorm

1. `docker pull node:16-bullseye`
2. Settings -> Languages & Frameworks -> Node.js
3. Node-Interpreter -> Add...
4. Add Remote...
5. Docker
6. Image name: node:16-bullseye
7. Node.js interpreter path: node
8. OK
9. Select the newly added interpreter
10. OK
11. Create regular npm run configuration. Add `FONTAWESOME_NPM_AUTH_TOKEN` to environment
12. In the run configuration: "Browse" docker container settings
13. Add port binding for port 8080 from the container to port 8080 on the host.

### Local project setup
```
FONTAWESOME_NPM_AUTH_TOKEN=<token> npm install
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Localization

Done using [Vue I18n](https://vue-i18n.intlify.dev/) (v8!).

Locale files are in [src/locales](src/locales)

### Simple strings
```
{{$t('label')}}
```

### Using slots
```
<i18n path="info" tag="p">
 <template #limit>
   <span>{{ changeLimit }}</span>
 </template>
 <template #action>
   <a :href="changeUrl">{{ $t('change') }}</a>
 </template>
</i18n>
```
