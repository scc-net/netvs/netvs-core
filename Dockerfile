FROM node:16-bullseye as netvs_dev
EXPOSE 8080
RUN apt-get -y update && apt-get install -y chromium firefox-esr wget
WORKDIR .
RUN mkdir -p /netvs
# Copy npm config and set token to enable FontAwesome-Pro access
ARG FONTAWESOME_NPM_AUTH_TOKEN
ENV FONTAWESOME_NPM_AUTH_TOKEN $FONTAWESOME_NPM_AUTH_TOKEN
RUN npm install -g npm@^9.6.2
RUN echo "npm install && npm run serve" > /run.sh
WORKDIR /netvs
ENTRYPOINT ["bash", "/run.sh"]

FROM netvs_dev as netvs_shell
ENTRYPOINT ["bash","-l"]
